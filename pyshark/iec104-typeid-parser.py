#
# (C) 2022, Martin Scheu, switch.ch
# IEC 60870-5-104 parser
# Reading TypeIDs and counting them
# Reading TypeID transitions and couting them
# Export parsed data as .cvs file
#




import pyshark
import csv
from fractions import Fraction
import time
import pprint


starttime = time.time()

sampleNumber = "01"

workingDir = '//Users/tinu/Downloads/'
pcapFileName = 'sample-iec104.pcap'

exportListName = 'export' + sampleNumber + '.csv'


def APCI_Type(cByte):
    # U-Type
    if (cByte & 2 > 0) and (cByte & 1 > 0) :
        result = "U"
    # S-Type
    elif ((cByte & 1) > 0) :
        result = "S"
    # I-Type
    elif (cByte & 1 == 0) :
        result = "I"
    else :
        result = "err10"

    return result

def UType(cByte):
    if (cByte & 128 > 0) and (cByte & 64 == 0):
        result = "TESTFR con"
    elif (cByte & 128 == 0) and (cByte & 64 > 0):
        result = "TESTFR act"

    elif (cByte & 32 > 0) and (cByte & 16 == 0):
        result = "STOPDT con"
    elif (cByte & 32 == 0) and (cByte & 16 > 0):
        result = "STOPDT act"

    elif (cByte & 8 > 0) and (cByte & 4 == 0):
        result = "STARTDT con"
    elif (cByte & 8 == 0) and (cByte & 4 > 0):
        result = "STARTDT act"
    else :
        result = "err20"
    return result


def from_iee756(byte1,byte2,byte3, byte4):

    k = 23
    mantisse = 0
    i = 0

    for i in range(23):
      if (((int(byte2,16) & 127) << 16 | int(byte3,16) << 8 | int(byte4,16)) >> i) & 1: mantisse = mantisse + Fraction(1,2**k)
      k -= 1

    if (int(byte1,16) & 64):
      base = 2
    else: base = 0

    result = (base**(((int(byte1,16) & 63  ) << 1 | (int(byte2,16) >> 7) & 1) + 1)) * (mantisse + 1)
    
    return (result.numerator / result.denominator)




# ----------- end functions -----------------



try : 
  pcap = pyshark.FileCapture(workingDir + pcapFileName)
except :
  print ("can not open", pcapFileName, " from ", workingDir )
else: 
  print (pcapFileName + " opened successfully")


TypeID_list = dict()
TypeID_transition_list = dict()


# data list header
header = ['Frame #', 'Sniff Time', 'Flags', 'SrcID','SrcPort', 'DstID', 'DstPort','Expert_message' ,'Type', 'TypeID', 'TypeID_count']

try: 
  csvFile = open(workingDir + exportListName, 'w', encoding='UTF8', newline='')
  writer = csv.writer(csvFile)
  # write the header
  writer.writerow(header)

except :
  print ("can not open " + exportListName + " in " + workingDir)
else: 
  print (exportListName + " opened successfully")


APDU = dict()
pac = dict()




macNmbr = 1
ipNmbr = 1

useIDs = True

typeIDold = 0

transitionStr = ''


for packet in pcap:


  # read and convert packet timestamp  
  ts_msec = int(packet.sniff_timestamp[-9:], 10)
  ts_sec = int(packet.sniff_timestamp[:len(packet.sniff_timestamp)-10], 10)
  pac['timestamp'] = '{}.{}'.format(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(ts_sec)), ts_msec)




  pac['layers'] = packet.layers



  #if 'ETH' in str(pac['layers']) and "IP" in str(pac['layers']) and "TCP" in str(pac['layers']): 
  if "IP" in str(pac['layers']) and "TCP" in str(pac['layers']): 

    #'flags_ack', 'flags_cwr', 'flags_ecn', 'flags_fin', 'flags_ns', 'flags_push', 'flags_res', 'flags_reset', 'flags_str', 'flags_syn', 'flags_urg',
  
    # create ID list, containing original IP and MAC address, create ID for each IP address
    pac['SrcIP_ID'] = packet.ip.src
    pac['DstIP_ID'] = packet.ip.dst
 


    #print(pktNmbr)
    

    #print( packet)
    pac['Expert_message'] = ''
    try:
      #print("packet nr :", packet.number, " with ", packet.tcp._ws_expert_message)
      pac['Expert_message'] = packet.tcp._ws_expert_message
    except AttributeError as e:
      pass

    #print('---')
    
    pac['payload_len'] = int(packet.tcp.len, 10)
    pac['flags'] = packet.tcp.flags

    pac['srcport'] = int(packet.tcp.srcport, 10)
    pac['dstport'] = int(packet.tcp.dstport, 10)

    if pac['payload_len'] > 1 : # TCP packet contains payload

      pac['startByte'] = int(packet.tcp.payload[0:2],16)

      if (pac['srcport'] == 2404 or pac['srcport'] == 2405 or pac['dstport'] == 2404 or pac['dstport'] == 2405) and pac['startByte'] == 104 :
     
        APDU_StartPos = [0]
        APDU_length = [0]

        currentAPDU = 1
        currentAPDU_StartPos =0

        # loop through TCP payload and read length of each APDU
        while (currentAPDU_StartPos < pac['payload_len']*3-1):
            APDU_StartPos.append(currentAPDU_StartPos)
            APDU_length.append(int(packet.tcp.payload[currentAPDU_StartPos+3:currentAPDU_StartPos + 3 + 2],16) & 255)
            currentAPDU_StartPos = currentAPDU_StartPos +5 + APDU_length[currentAPDU] * 3 + 1

            APDU['Type'] = APCI_Type(int(packet.tcp.payload[APDU_StartPos[currentAPDU]+6:APDU_StartPos[currentAPDU] + 6 + 2],16) & 255)
            APDU['APDU_Num'] = currentAPDU

            # get cause of transmission COT
            if APDU_length[currentAPDU] > 8 :
                APDU['CauseTx'] = int(packet.tcp.payload[APDU_StartPos[currentAPDU]+24:APDU_StartPos[currentAPDU] + 24 + 2],16) & 63
            else :
                APDU['CauseTx'] = 0

            # get TypeID, only when available
            if (APDU_length[currentAPDU] > 6 ): 
                APDU['TypeID'] = int(packet.tcp.payload[APDU_StartPos[currentAPDU]+18:APDU_StartPos[currentAPDU] + 18 + 2],16) & 255

                if APDU['TypeID'] in TypeID_list.keys() :       
                    TypeID_list[APDU['TypeID']] = TypeID_list[APDU['TypeID']] + 1
                else:
                    TypeID_list[APDU['TypeID']] = 1
                if typeIDold != 0:
                    transitionStr = str(typeIDold) + ' - ' + str(APDU['TypeID'])

                    if transitionStr in TypeID_transition_list.keys() :
                        TypeID_transition_list[transitionStr] = TypeID_transition_list[transitionStr] + 1
                    else:
                        TypeID_transition_list[transitionStr] = 1
                        #print('TypeID transition', str(typeIDold), ' - ' ,str(APDU['TypeID']), ' at ', packet.number)
                    
                typeIDold = APDU['TypeID']
                data = [packet.number, pac['timestamp'],pac['flags'], pac['SrcIP_ID'], pac['srcport'], pac['DstIP_ID'], pac['dstport'],pac['Expert_message'], APDU['Type'], APDU['TypeID'], TypeID_list[APDU['TypeID']], transitionStr ]
            else :
                APDU['TypeID'] = 0 
                data = [packet.number, pac['timestamp'],pac['flags'], pac['SrcIP_ID'], pac['srcport'], pac['DstIP_ID'], pac['dstport'],pac['Expert_message'], APDU['Type'], APDU['TypeID'], 0 ]

            writer.writerow(data)

            currentAPDU += 1

        # end while loop (currentAPDU_StartPos < pac['payload_len']*3-1)

    else:
      data = [packet.number, pac['timestamp'],pac['layers'],pac['flags'], pac['SrcIP_ID'], pac['srcport'], pac['DstIP_ID'], pac['dstport'],pac['Expert_message']]
      writer.writerow(data)
  



  #end while loop pktNmbr < len(pcap)

csvFile.close()
print('data disected and ' + exportListName + ' file written')

print('')
print('TypeIDs found in pcap: ')
print('{TypeID : Recourrences}')
pprint.pprint(TypeID_list)
print('TypeID transitions found in pcap:')
print('{Transition TypeID - TypeID : Recourrences}')
pprint.pprint(TypeID_transition_list)




endtime = time.time()
print(endtime - starttime)

exeTime = time.strftime('%H:%M:%S', time.gmtime(endtime - starttime))
print(exeTime)









