# sharkfest22

Advanced IEC 60870-5-104 analysis with Wireshark

## Getting started

For the hands on part you need:
1. Wireshark 4.0 or newer
2. Sample IEC-104 pcap : https://github.com/automayt/ICS-pcap/blob/master/IEC%2060870/iec104/iec104.pcap
3. Wireshark filter set for IEC 60870-5-104 : https://gitlab.switch.ch/martin.scheu/sharkfest22/-/blob/main/wireshark-filter/IEC60870-5-104.zip
   ( credits to Lenny, I've used his filter as a base: https://networkforensic.dk/Tools/default.html )
4. Python3
5. pyshark (https://pypi.org/project/pyshark/ install it with `pip3 install pyshark` )
6. Analysis script : https://gitlab.switch.ch/martin.scheu/sharkfest22/-/raw/main/pyshark/iec104-typeid-parser.py

For the second part:
1. Industroyer2 pcap files : https://www.netresec.com/files/Industroyer2-Netresec.zip

Usefull links:
1. TypeID overview: https://infosys.beckhoff.com/content/1033/tf6500_tc3_iec60870_5_10x/984447883.html?id=4468958939044453278

Slides
https://gitlab.switch.ch/martin.scheu/sharkfest22/-/blob/main/SF22EU-Scheu-Martin.pdf
